//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse
    
    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .get
    }
}
