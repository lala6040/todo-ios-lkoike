//
//  APIClient.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/04.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Alamofire

final class APIClient {
    private let unexpectedErrorMessage = "サーバー内で不明なエラーが発生しました"
    private var commonDecoder: JSONDecoder {
        let formatter = DateFormatter()
        let decoder = JSONDecoder()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        decoder.dateDecodingStrategy = .formatted(formatter)
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }

    func call<T: RequestProtocol>(request: T, success: @escaping (T.Response) -> Void, failure: @escaping (String) -> Void) {
        let requestUrl = request.baseUrl + request.path

        AF.request(requestUrl,
                   method: request.method,
                   parameters: request.parameters,
                   encoding: request.encoding,
                   headers: request.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                guard let data = response.data else {
                    failure(self.unexpectedErrorMessage)
                    return
                }
                switch response.result {
                case .success(let data):
                    do {
                        let result = try self.commonDecoder.decode(T.Response.self, from: data)
                        success(result)
                    } catch {
                        failure(self.unexpectedErrorMessage)
                    }
                case .failure:
                    let result = try? self.commonDecoder.decode(CommonResponse.self, from: data)
                    let message = result?.errorMessage ?? self.unexpectedErrorMessage
                    failure(message)
                }
        }
    }
}
