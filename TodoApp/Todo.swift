//
//  Todo.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
