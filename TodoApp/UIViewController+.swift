//
//  UIViewController+.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlertDialog(message: String) {
        let alertController = UIAlertController(title: "エラー", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "閉じる", style: .default))
        present(alertController, animated: true)
    }
}
