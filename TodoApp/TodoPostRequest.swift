//
//  TodoPostRequest.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/07.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Alamofire

struct TodoPostRequest: RequestProtocol {
    typealias Response = CommonResponse

    let title: String
    let detail: String?
    let date: Date?

    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .post
    }
    var parameters: Parameters? {
        var parameters = ["title": title]
        if let detail = detail {
            parameters["detail"] = detail
        }
        if let date = date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
