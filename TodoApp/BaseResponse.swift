//
//  BaseResponse.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
