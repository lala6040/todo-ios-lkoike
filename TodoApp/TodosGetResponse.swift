//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Foundation

struct TodosGetResponse: Codable {
    let todos: [Todo]
    let errorCode: Int
    let errorMessage: String
}
