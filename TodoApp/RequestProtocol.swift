//
//  RequestProtocol.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/04.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Alamofire

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        return "https://test-todo-api-leokoike.herokuapp.com/"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
