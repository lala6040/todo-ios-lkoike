//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by lkoike on 2020/07/29.
//  Copyright © 2020 lkoike. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?

    var todo: Todo?

    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var dateTextField: UITextField!

    private let titleCharacterLimit = 100
    private let detailCharacterLimit = 1000
    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()

    private var titleCharacterCount: Int {
        titleTextField.text?.count ?? 0
    }
    private var detailCharacterCount: Int {
        detailTextView.text.count
    }
    private var isTitleTextOverLimit: Bool {
        titleCharacterCount > titleCharacterLimit
    }
    private var isDetailTextOverLimit: Bool {
        detailCharacterCount > detailCharacterLimit
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItem()
        setUpDetailTextView()
        setUpRegistrationButton()
        setUpDatePicker()
        dateFormatter.dateFormat = "yyyy/M/d"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        setUpView()
        if let todo = todo {
            setUpTexts(todo: todo)
        }
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }

    private func setUpDetailTextView() {
        detailTextView.layer.cornerRadius = 5.0
        detailTextView.layer.borderColor = UIColor.gray.cgColor
        detailTextView.layer.borderWidth = 0.5
        detailTextView.delegate = self
    }

    private func setUpRegistrationButton() {
        registrationButton.layer.cornerRadius = 5.0
        registrationButton.layer.borderWidth = 0.5
        changeRegistrationButtonState()
        if todo != nil {
            registrationButton.setTitle("更新", for: .normal)
        }
    }

    private func setUpDatePicker() {
        datePicker.calendar = .init(identifier: .gregorian)
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current

        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(onTappedDoneButton))
        let deleteItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(onTappedDeleteButton))
        let cancelItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(onTappedCancelButton))
        toolbar.setItems([deleteItem, spaceItem, cancelItem, doneItem,], animated: true)

        dateTextField.inputView = datePicker
        dateTextField.inputAccessoryView = toolbar
    }

    private func setUpView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    private func changeRegistrationButtonState() {
        if titleCharacterCount == 0 || isTitleTextOverLimit || isDetailTextOverLimit {
            registrationButton.backgroundColor = .gray
            registrationButton.isEnabled = false
        } else {
            registrationButton.backgroundColor = .blue
            registrationButton.isEnabled = true
        }
    }

    @objc private func onTappedDoneButton() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func onTappedDeleteButton() {
        dateTextField.endEditing(true)
        dateTextField.text = nil
    }

    @objc private func onTappedCancelButton() {
        dateTextField.endEditing(true)
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    @IBAction private func onChangedTitleTextCharacter(_ sender: Any) {
        titleCountLabel.text = String(titleCharacterCount)
        titleCountLabel.textColor = isTitleTextOverLimit ? .red : .black
        changeRegistrationButtonState()
    }

    @IBAction private func onTappedRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        if let id = todo?.id {
            updateTodo(id: id, title: title, detail: detail, date: date)
        } else {
            sendTodo(title: title, detail: detail, date: date)
        }
    }

    private func sendTodo(title: String, detail: String?, date: Date?) {
        APIClient().call(
            request: TodoPostRequest(title: title, detail: detail, date: date),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "登録しました")
            }, failure: { [weak self] message in
                self?.showErrorAlertDialog(message: message)
            }
        )
    }

    private func updateTodo(id: Int, title: String, detail: String?, date: Date?) {
        let todo = Todo(id: id, title: title, detail: detail, date: date)
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "更新しました")
            }, failure: { [weak self] message in
                self?.showErrorAlertDialog(message: message)
            }
        )
    }
    
    private func setUpTexts(todo: Todo) {
        titleCountLabel.text = String(todo.title.count)
        detailCountLabel.text = String(todo.detail?.count ?? 0)
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }
}
extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        detailCountLabel.text = String(detailCharacterCount)
        detailCountLabel.textColor = isDetailTextOverLimit ? .red : .black
        changeRegistrationButtonState()
    }
}
