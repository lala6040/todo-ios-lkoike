//
//  CommonResponse.swift
//  TodoApp
//
//  Created by lkoike on 2020/08/05.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    let errorCode: Int
    let errorMessage: String
}
