//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by lkoike on 2020/07/21.
//  Copyright © 2020 lkoike. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {

    @IBOutlet private weak var todoTableView: UITableView!
    @IBOutlet private weak var registerButton: UIBarButtonItem!

    private var todoList: [Todo] = []
    private var isDeleteMode = false

    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        setUpNavigationItem()
        fetchTodoList()
    }

    @IBAction private func onTappedRegisterButton(_ sender: Any) {
        goToEditView()
    }
    
    @IBAction private func onTappedTrashButton(_ sender: Any) {
        isDeleteMode.toggle()
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let deleteButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(onTappedTrashButton(_:)))
        navigationItem.rightBarButtonItems = [deleteButton, registerButton]
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todoList = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] message in
                self?.showErrorAlertDialog(message: message)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlertDialog(message: message)
            }
        )
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func goToEditView(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditView = storyboard.instantiateViewController(withIdentifier: "Edit") as? TodoEditViewController else { return }
        todoEditView.delegate = self
        todoEditView.todo = todo
        navigationController?.pushViewController(todoEditView, animated: true)
    }

    private func showDeleteTodoAlert(todo: Todo) {
        let alertController = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか？", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        let okButtonAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        }
        alertController.addAction(okButtonAction)
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = todoList[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let todo = todoList[indexPath.row]
        if isDeleteMode {
            showDeleteTodoAlert(todo: todo)
        } else {
            goToEditView(todo: todo)
        }
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}
