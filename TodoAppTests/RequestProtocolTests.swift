//
//  RequestProtocolTests.swift
//  TodoAppTests
//
//  Created by lkoike on 2020/09/18.
//  Copyright © 2020 lkoike. All rights reserved.
//

import XCTest

final class RequestProtocolTests: XCTestCase {
    
    func testInit() {
        let request = TestRequestProtocol()
        
        XCTAssertNil(request.parameters)
        XCTAssertEqual(request.baseUrl, "https://test-todo-api-leokoike.herokuapp.com/")
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertEqual(request.encoding.toJsonEncoding(), .default)
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "/todos")
    }
}
