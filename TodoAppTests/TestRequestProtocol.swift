//
//  TestRequestProtocol.swift
//  TodoAppTests
//
//  Created by lkoike on 2020/10/02.
//  Copyright © 2020 lkoike. All rights reserved.
//

import Alamofire
@testable import TodoApp

final class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse
    
    var path: String {
        "/todos"
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var parameters: Parameters? {
        nil
    }
    
    var baseUrl: String {
        "https://test-todo-api-leokoike.herokuapp.com/"
    }
    
    var encoding: ParameterEncoding {
        JSONEncoding.default
    }
    
    var headers: HTTPHeaders? {
        ["Content-Type": "application/json"]
    }
}

extension ParameterEncoding {
    func toJsonEncoding() -> JSONEncoding? {
        self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    public static func == (lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        lhs.options == rhs.options
    }
}
