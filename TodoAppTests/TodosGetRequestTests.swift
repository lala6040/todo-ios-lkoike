//
//  TodosGetRequestTests.swift
//  TodoAppTests
//
//  Created by lkoike on 2020/09/18.
//  Copyright © 2020 lkoike. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodosGetRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = TodosGetRequest()
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "todos")
    }
    
    func testResponse() {
        var todosGetResponse: TodosGetResponse?
        
        stub(condition: isHost("test-todo-api-leokoike.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("TodosGetResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Get Todo Request")
        
        APIClient().call(
            request: TodosGetRequest(),
            success: { response in
                todosGetResponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosGetResponse)
            XCTAssertEqual(todosGetResponse?.errorCode, 0)
            XCTAssertEqual(todosGetResponse?.errorMessage, "")
            XCTAssertEqual(todosGetResponse?.todos.count, 3)
            XCTAssertEqual(todosGetResponse?.todos[0].title, "Leo")
            XCTAssertEqual(todosGetResponse?.todos[0].detail, "Koike")
            XCTAssertEqual(todosGetResponse?.todos[0].date, "2020-09-29T15:00:00.000Z".toDate())
            XCTAssertEqual(todosGetResponse?.todos[1].title, "lion")
            XCTAssertEqual(todosGetResponse?.todos[1].detail, "gaogao")
            XCTAssertNil(todosGetResponse?.todos[1].date)
            XCTAssertEqual(todosGetResponse?.todos[2].title, "tategami")
            XCTAssertNil(todosGetResponse?.todos[2].detail)
            XCTAssertNil(todosGetResponse?.todos[2].date)
        }
    }
}
extension String {
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        return dateFormatter.date(from: self)
    }
}
