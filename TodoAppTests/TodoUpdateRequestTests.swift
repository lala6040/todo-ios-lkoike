//
//  TodoUpdateRequestTests.swift
//  TodoAppTests
//
//  Created by lkoike on 2020/09/18.
//  Copyright © 2020 lkoike. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoUpdateRequestTests: XCTestCase {
    
    private let todo = Todo(id: 1, title: "Leo", detail: nil, date: nil)
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = TodoUpdateRequest(todo: todo)
        
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "todos/1")
    }
    
    func testResponse() {
        var todoUpdateResponse: CommonResponse?
        
        stub(condition: isHost("test-todo-api-leokoike.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Update Todo Request")
        
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { response in
                todoUpdateResponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoUpdateResponse)
            XCTAssertEqual(todoUpdateResponse?.errorCode, 0)
            XCTAssertEqual(todoUpdateResponse?.errorMessage, "")
        }
    }
}
