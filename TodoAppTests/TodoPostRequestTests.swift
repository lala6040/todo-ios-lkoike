//
//  TodoPostRequestTests.swift
//  TodoAppTests
//
//  Created by lkoike on 2020/09/18.
//  Copyright © 2020 lkoike. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPostRequestTests: XCTestCase {
    let postRequest = TodoPostRequest(title: "Koike", detail: nil, date: nil)

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = postRequest
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "todos")
    }
    
    func testResponse() {
        var todosPostResponse: CommonResponse?
        
        stub(condition: isHost("test-todo-api-leokoike.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Post Todo Request")
        
        APIClient().call(
            request: postRequest,
            success: { response in
                todosPostResponse = response
                expectation.fulfill()
        },
            failure: { _ in
                return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosPostResponse)
            XCTAssertEqual(todosPostResponse?.errorCode, 0)
            XCTAssertEqual(todosPostResponse?.errorMessage, "")
        }
    }
}
